# OpenML dataset: yeast

https://www.openml.org/d/40597

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multi-label dataset. The yeast dataset (Elisseeff and Weston, 2002) consists of micro-array expression data, as well as phylogenetic profiles of yeast, and includes 2417 genes and 103 predictors. In total, 14 different labels can be assigned to a gene, but only 13 labels were used due to label sparsity.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40597) of an [OpenML dataset](https://www.openml.org/d/40597). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40597/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40597/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40597/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

